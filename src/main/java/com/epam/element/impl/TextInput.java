package com.epam.element.impl;

import com.epam.element.PageElement;
import com.epam.utils.Waiter;
import org.openqa.selenium.WebElement;

public class TextInput extends PageElement {
    public TextInput(WebElement element) {
        super(element);
    }

    public void inputText(CharSequence... text) {
        Waiter.visibilityOf(element);
        element.clear();
        element.sendKeys(text);
    }
}
