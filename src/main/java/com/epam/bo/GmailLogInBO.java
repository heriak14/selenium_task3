package com.epam.bo;

import com.epam.po.impl.EmailInputPage;
import com.epam.po.impl.PasswordInputPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GmailLogInBO {
    private static Logger logger = LogManager.getLogger();
    private EmailInputPage emailPage;
    private PasswordInputPage passwordPage;

    public GmailLogInBO() {
        emailPage = new EmailInputPage();
        passwordPage = new PasswordInputPage();
    }

    public void login(String email, String password) {
        logger.info("logging as " + email);
        emailPage.enterEmail(email);
        emailPage.clickNextButton();
        passwordPage.enterPassword(password);
        passwordPage.clickNextButton();
    }

    public boolean isSuccessfullyLoggedIn(String email) {
        return passwordPage.checkIfTitleContains(email);
    }
}
