package com.epam.po.impl;

import com.epam.element.impl.Button;
import com.epam.element.impl.CheckBox;
import com.epam.po.BasePage;
import com.epam.utils.Waiter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class IncomingMessagesPage extends BasePage {
    private static final Logger LOGGER = LogManager.getLogger();
    @FindBy(xpath = "//tr[contains(@class, 'zA')]//td[@class='apU xY']//span[contains(@class, 'T-KT')][@role='button']")
    private List<CheckBox> starCheckBoxes;
    @FindBy(xpath = "//div[@class='aim']//span[@class='nU ']//a[contains(@href, '#starred')]")
    private Button importantMessagesButton;

    public boolean areCheckBoxesSelected(int number) {
        LOGGER.info("checking if " + number + " checkboxes are selected");
        return starCheckBoxes.stream()
                .limit(number)
                .map(e -> e.getAttribute("class"))
                .allMatch(c -> c.contains("T-KT-Jp"));
    }

    public void markMessagesAsImportant(int number) {
        LOGGER.info("marking " + number + " messages as starred");
        starCheckBoxes.stream()
                .limit(number)
                .forEach(CheckBox::setChecked);
    }

    public void switchToImportantMessages() {
        LOGGER.info("switching to starred messages folder");
        importantMessagesButton.waitAndClick();
        Waiter.titleContains("Starred");
    }
}
