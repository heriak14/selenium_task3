package com.epam.po.impl;

import com.epam.element.impl.Button;
import com.epam.element.impl.TextInput;
import com.epam.po.BasePage;
import com.epam.utils.Waiter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.FindBy;

public class PasswordInputPage extends BasePage {
    private static final Logger LOGGER = LogManager.getLogger();
    @FindBy(css = "#password div.aCsJod.oJeWuf div div.Xb9hP input")
    private TextInput passwordField;
    @FindBy(id = "passwordNext")
    private Button nextButton;

    public void enterPassword(String password) {
        LOGGER.info("entering password");
        passwordField.inputText(password);
    }

    public void clickNextButton() {
        LOGGER.info("clicking next button");
        nextButton.waitAndClick();
    }

    public boolean checkIfTitleContains(String str) {
        try {
            Waiter.titleContains(str);
            return true;
        } catch (TimeoutException e) {
            return false;
        }
    }
}
