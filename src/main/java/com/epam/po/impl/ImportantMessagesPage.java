package com.epam.po.impl;

import com.epam.element.impl.Button;
import com.epam.element.impl.CheckBox;
import com.epam.po.BasePage;
import com.epam.utils.Waiter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ImportantMessagesPage extends BasePage {
    private static final Logger LOGGER = LogManager.getLogger();
    @FindBy(css = "div[role='main'] td.oZ-x3.xY")
    private List<CheckBox> messageCheckBoxes;
    @FindBy(css = "div.D.E.G-atb[gh] div.T-I.J-J5-Ji.nX.T-I-ax7.T-I-Js-Gs.mA")
    private Button deleteButton;
    @FindBy(id = "link_undo")
    private Button undoButton;

    public int getCheckBoxesNumber() {
        return messageCheckBoxes.size();
    }

    public void selectMessages(int number) {
        LOGGER.info("selecting " + number + " messages");
        messageCheckBoxes.stream()
                .limit(number)
                .forEach(CheckBox::setChecked);
    }

    public void clickDeleteButton() {
        LOGGER.info("clicking 'delete' button");
        deleteButton.waitAndClick();
    }

    public boolean isUndoButtonVisible() {
        try {
            Waiter.visibilityOf(undoButton);
            return true;
        } catch (TimeoutException e) {
            return false;
        }
    }

    public void clickUndoButton() {
        undoButton.click();
    }
}
