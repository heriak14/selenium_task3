package com.epam.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.stream.Stream;

public class CSVParser {
    private static final String COMMA_DELIMITER = ", *";

    public static Iterator<Object[]> parse(String csvPath) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(csvPath))) {
            Stream.Builder<Object[]> builder = Stream.builder();
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(COMMA_DELIMITER);
                builder.add(values);
            }
            return builder.build().iterator();
        }
    }
}
