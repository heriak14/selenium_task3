package com.epam.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Properties;

public enum Property {
    DRIVER("config.properties");

    private Properties properties;

    Property(String fileName) {
        properties = new Properties();
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream(fileName));
        } catch (IOException e) {
            LogManager.getLogger().error(e.getMessage());
        }
    }

    public String getProperty(String name) {
        return properties.getProperty(name);
    }
}
