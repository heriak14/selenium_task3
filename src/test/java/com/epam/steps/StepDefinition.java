package com.epam.steps;

import com.epam.bo.GmailLogInBO;
import com.epam.bo.ImportantMessagesBO;
import com.epam.listener.TestListener;
import com.epam.utils.DriverProvider;
import com.epam.utils.Property;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.annotations.Listeners;

import static org.testng.Assert.assertTrue;

public class StepDefinition {
    private GmailLogInBO logInBO;
    private ImportantMessagesBO importantMessagesBO;

    @Given("^User is on Gmail LogIn page$")
    public void userIsOnGmailLogInPage() {
        DriverProvider.getDriver().get(Property.DRIVER.getProperty("page.base"));
    }

    @When("^User log in as \"([^\"]*)\", \"([^\"]*)\"$")
    public void userLogInAs(String email, String password) {
        logInBO = new GmailLogInBO();
        logInBO.login(email, password);
    }

    @Then("^Page title contains user email \"([^\"]*)\"$")
    public void pageTitleContainsUserEmail(String email) {
        assertTrue(logInBO.isSuccessfullyLoggedIn(email), "logging in Gmail failed");
    }

    @When("^User mark (\\d+) messages as important$")
    public void userMarkMessagesMessagesAsImportant(int messagesNum) {
        importantMessagesBO = new ImportantMessagesBO();
        importantMessagesBO.moveMessagesToImportant(messagesNum);
    }

    @Then("^(\\d+) messages are moved to important folder$")
    public void messagesMessagesAreMovedToImportantFolder(int messagesNum) {
        assertTrue(importantMessagesBO.areMessagesMovedToImportant(messagesNum), "messages wasn't moved to important");
    }

    @When("^User deletes (\\d+) messages from important folder$")
    public void userDeletesMessagesMessagesFromImportantFolder(int messagesNum) {
        importantMessagesBO.deleteMessagesFromImportant(messagesNum);
    }

    @Then("^(\\d+) messages are deleted$")
    public void messagesMessagesAreDeleted(int messagesNum) {
        assertTrue(importantMessagesBO.areImportantMessagesRemoved(messagesNum), "messages wasn't removed");
        importantMessagesBO.undoDeleting();
    }

    @And("^WebDriver is closed$")
    public void webdriverIsClosed() {
        DriverProvider.closeDriver();
    }
}
