package com.epam.listener;

import com.epam.utils.DriverProvider;
import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.internal.BaseTestMethod;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.stream.Collectors;

public class TestListener implements ITestListener {

    @Override
    public void onTestStart(ITestResult result) {
        try {
            BaseTestMethod baseTestMethod = (BaseTestMethod) result.getMethod();
            Field f = baseTestMethod.getClass().getSuperclass().getDeclaredField("m_methodName");
            String originMethodName = result.getMethod().getMethodName().split("_")[0];
            String parameters = Arrays.stream(result.getParameters())
                    .map(Object::toString)
                    .collect(Collectors.joining(", "));
            f.setAccessible(true);
            f.set(baseTestMethod, originMethodName + "_" + parameters);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            Reporter.log(e.getMessage());
        }
    }

    @Override
    public void onTestFailure(ITestResult result) {
        addTextLog("Test fell on method " + result.getMethod().getConstructorOrMethod().getName());
        addScreenShot(DriverProvider.getDriver());
    }

    @Attachment(value = "Fail page screenshot", type = "image/png")
    private byte[] addScreenShot(WebDriver driver) {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    @Attachment(value = "{0}", type = "text/plain")
    private String addTextLog(String log) {
        return log;
    }
}
