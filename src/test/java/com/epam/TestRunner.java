package com.epam;

import com.epam.listener.TestListener;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.Listeners;

@Listeners({TestListener.class})
@CucumberOptions(
        features = "src/test/resources/features/gmail.feature",
        glue = {"com.epam.steps"})
public class TestRunner extends AbstractTestNGCucumberTests {
}
